import java.awt.Graphics;
import javax.swing.JPanel;

//extends enhanced type of JPanel (inheritance)
public class DrawPanel extends JPanel 
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void paintComponent (Graphics g)
	{	
		// renders screen
		super.paintComponent(g);
		
		int width = getWidth(); //public method of JPanel
		int height = getHeight(); // public method of JPanel
		
		//draw a line from upper-left to the lower right
		g.drawLine(0, 0, width, height); //((x,y),(x,y))
		//draw a line from lower left to upper right
		g.drawLine(0, height, width, 0);
				
	}
}
